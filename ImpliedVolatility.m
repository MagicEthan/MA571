function[impliedV]=ImpliedVolatility(S,K,r,t,Cobs,N,tol)

sigma0=sigmaStart(S,K,r,t);
% Use the Newton's Method to calculate Implied Volatility
n = 1;
sig = sigma0;
blc = blc_C_Vega(S,K,r,sig,t);
Cbs = blc(1);
vega = blc(2);
f = Cbs - Cobs;
while (abs(f) > tol && n<N)
    osig = sig;
    sig = osig - ((Cbs - Cobs)/vega);
    blc = blc_C_Vega(S,K,r,sig,t);
    Cbs = blc(1);
    vega = blc(2);
    f = Cbs - Cobs;
	n = n + 1;
    if (n == N-1 || abs(sig) == inf)
        sig = nan;
    end
end

impliedV = sig;


function sigma = sigmaStart(S,K,r,t)
    %get the initial starting sigma
    sigma = 0.001;
    blc = blc_C_Vega(S,K,r,sigma,t);
    Cbs = blc(1);
    vega = blc(2);
    f = Cbs - Cobs;
    while (f<0)
        sigma = sigma + 0.01;
        blc = blc_C_Vega(S,K,r,sigma,t);
        Cbs = blc(1);
     	vega = blc(2);
        f = Cbs - Cobs;
    end
end

end