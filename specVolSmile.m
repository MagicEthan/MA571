function[mat]=specVolSmile(N,tol)
%Calculate the implied volatility and draw the volitility smile
%Note: We used the last close price to calculate the implied volatility,
%for those deep in money calls, starting from a veral small starting sigma
%i.e. 0.001 may still lead to a positive value for f = Cbs - Cobs, thus actually 
%by Newton's approximation method the implied volatility we get may be infinity.
%Here I deleted the inf value and drew the volatility smile by fitting a 
%quadratic polynomial. 

disp('Choose the option you want to calculate, for example');
disp('Enter 1 for AAPL_Jan8_16, the American Call Options for Apple expired at Jan 8, 2016');
disp('Enter 2 for AAPL_Mar18_16, the American Call Options for Apple expired at Mar 18, 2016');
disp('Enter 3 for AAPL_Jul15_16, the American Call Options for Apple expired at July 15, 2016');
disp('Enter 4 for AMZN_Jan8_16, the American Call Options for Amazon expired at Jan 8, 2016');
disp('Enter 5 for AMZN_Feb19_16, the American Call Options for Amazon expired at Feb 19, 2016');
disp('Enter 6 for AMZN_Jul15_16, the American Call Options for Amazon expired at July 15, 2016');
%These American Call Options are trading on CBOE

prompt1 = ['Please Enter the number',char(10)];
fn = input(prompt1);
if fn==1
    AAPL_Jan8_16  = xlsread('AAPL',1);
    M = AAPL_Jan8_16;
elseif fn==2
    AAPL_Mar18_16 = xlsread('AAPL',2);
    M = AAPL_Mar18_16;
elseif fn==3
    AAPL_Jul15_16 = xlsread('AAPL',3);
    M = AAPL_Jul15_16;
elseif fn==4
    AMZN_Jan8_16  = xlsread('AMZN',1);
    M = AMZN_Jan8_16;
elseif fn==5
    AMZN_Feb19_16 = xlsread('AMZN',2);
    M = AMZN_Feb19_16;
elseif fn==6
    AMZN_Jul15_16 = xlsread('AMZN',3);
    M = AMZN_Jul15_16;
else
    error('Please enter an integer for 1 to 6!');
end
    K = M(:,1);
    Cobs = M(:,2);
    S = M(1,3);
    r = M(2,3);
    t = M(3,3);

L = length(K);
IV = zeros(L,1);
for i=1:L
IV(i) = ImpliedVolatility(S,K(i),r,t,Cobs(i),N,tol);
end
mat = [IV,K];
nonan = find(~isnan(mat(:,1)));
mat = mat(nonan,:);
scatter(mat(:,2),mat(:,1));
hold;
plot( fit(mat(:,2),mat(:,1),'poly2'));
end
