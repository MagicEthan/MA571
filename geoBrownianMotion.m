%The description of this questoin is a little ambiguous, which can lead
%to different result.
%For a one-step geometric Brownian Motion from 0 to T, this will be quite simple.
%From the first function brownianMotion.m, we have already generated a Brwonian
%Motion on [0,T]. we can just use it and the result is as follows

function[ST]=geoBrownianMotion(mu,sigma,n,T,S0)
%[MT,WT]=brownianMotion(n,T);
%ST=S0*exp((mu-0.5*sigma^2)*T+sigma*WT);

%However, in case you want an N steps geo brownian motion, like generating
%the path of ST(i), I also wrote one. From the question I guess N=n*T,
%where n and T are both used for generating Brownian Motion and as the
%number of steps. However, I think it's better to introduce another new
%parameter N as the #steps, which has nothing to do with n and t.

N=n*T;
dt=1/N;
ST=zeros(N+1,1);
ST(1)=S0;
for i=2:N+1
    [MT,WT]=brownianMotion(n,T);%You have to call the first function N times to get different WT
    W=WT/sqrt(T);%Here W~N(0,1), a standard Brownian Motion
    ST(i)=ST(i-1)*exp((mu-0.5*sigma^2)*dt+sigma*sqrt(dt)*W);%increment in each step follows N(0,dt)
end

%And this is a n*T steps geometric Brownian Motion, which use the Brwonian
%Motion W as generated and adjusted by the first function

%Maybe some will directly write the function like this:
%[MT,WT]=brownianMotion(n,T);
%N=n*T;
%dt=1/N;
%ST=zeros(N+1,1);
%ST(1)=S0;
%for i=2:N+1
%    ST(i)= ST(i-1)*exp((mu-0.5*sigma^2)*dt+sigma*MT(i));
%end
%This is wrong, and will not give the results we want, because we iterate 
%by a too small time step 1/N. However, we can set 
%n extremely large, set i=1:n:N+1 and rewrite the iteration in
%another way. I'm not sure about the following codes.

%[MT,WT]=brownianMotion(n,T);
%t=0:0.1:T;
%count=length(t);
%ST=zeros(count,1);
%ST(1)=S0;
%for i=2:count
%    i1=floor((i-1)*0.1*n+1);
%    i0=floor((i-2)*0.1*n+1);
%    ST(i)= ST(i-1)*exp((mu-0.5*sigma^2)*0.1+sigma*(MT(i1)-MT(i0)));
%end