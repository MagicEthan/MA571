%Calculate American Put by binomial trees process, and show the tree.
function[]=americanPut(u,d,r,N,S0,K)

if ( u<0 || r<0 || u<0 || d>u || 1+r<=d || 1+r>=u ) 
error('Please enter the parameters so that no arbitrage happens.');
end

p=riskFreeP(u,d,r);
ph=p(1);
pt=p(2);
tree=BTree(N,S0,u,d);
V=mktree(N+1,1,S0);
for m=1:N+1
V{N+1}(m)=max((K-tree{N+1}(m)),0);
end
for p=N:-1:1
    for q=1:p
	V{p}(q)=max(max((K-tree{p}(q)),0),((ph*V{p+1}(q)+pt*V{p+1}(q+1))/(1+r)));
    end
end

celldisp(V);

function p=riskFreeP(u,d,r)
ph=(1+r-d)/(u-d);
pt=(u-1-r)/(u-d);
p=[ph,pt];
end

function tree=BTree(N,S0,u,d)
tree=mktree(N+1,1,S0);
for i=2:N+1
    tree{i}(1)=tree{i-1}(1)*u;
    for j=2:i
    tree{i}(j)=tree{i-1}(j-1)*d;
    end
end
end

end


