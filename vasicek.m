function[rt]=vasicek(n,T,r0,sigma,alpha,beta)
[MT,WT]=brownianMotion(n,T);
N=n*T;%#steps N
dt=1/N;
ST=zeros(N+1,1);
rt=zeros(N+1,1);
drt=zeros(N,1);
rt(1)=r0;
for i=1:N
    [MT,WT]=brownianMotion(n,T);
    W=WT/sqrt(T);%Standard Brownian Motion
    drt(i)=alpha*(beta-rt(i))*dt+sigma*sqrt(dt)*W;
    rt(i+1)=rt(i)+drt(i);
end