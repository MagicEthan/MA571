%Generate the approximation of Brownian Motion W(T)
%Note that: W(T)~N(0,T) when n goes into infinity
function[MT,WT]=brownianMotion(n,T)
N=n*T;
X=zeros(N+1,1);
for i=2:N+1
    toss=randperm(2);
        if toss(1)==1
            X(i)=1;
        else
            X(i)=-1;
        end
end
MT=zeros(N+1,1);
nRoot=sqrt(n);
for i=2:N+1
    MT(i)=MT(i-1)+X(i)/nRoot;
end
%MT(i) is the path of the scaled symmetric random walk over [0,T], when n
%goes into infinity, it approximates a Brownian Motion.
WT=MT(N+1);
%WT follows the normal distribution of mean zero and variance T
%Or furthermore, W=WT/sqrt(T)~N(0,1);
   
