function[blc] = blc_C_Vega(S,K,r,sigma,t)
    %Calculate the price for American/European Call options by Black Scholes
    d1 = (log(S/K)+(r+0.5*sigma^2)*t)/(sigma*sqrt(t));
    d2 = d1 - sigma*sqrt(t);
    N1 = 0.5*(1+erf(d1/sqrt(2)));
    N2 = 0.5*(1+erf(d2/sqrt(2)));
    diffN1 = normpdf(d1);
    
    vega = S*diffN1*sqrt(t);
    C = S*N1 - K*exp(-r*t)*N2;
    
    blc = [C,vega];
end