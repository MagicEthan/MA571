function[]=binomialTree()

%It's very convenient to use the mktree function to create a recombining tree,
%however, this funtion uses cell as the default data structure, which
%makes it not aesthetically pleasing to read.

%tree=mktree(N+1,1,S0);
%for i=2:N+1
%    tree{i}(1)=tree{i-1}(1)*u;
%    for j=2:i
%    tree{i}(j)=tree{i-1}(j-1)*d;
%    end
%end

%celldisp(tree)

%The visualization of this recombining tree cannot be implemented in an
%easy way without Matlab financial toolkits or other special tree visulization
%toolkits. As a beginner and due to the limited time, to make the GUI more
%friendly, I'll create a matrix as a pseudo binomial tree, which looks
%like a real binomial tree.

%Calculate and display risk neutral probability
disp('Let''s first calculate the risk neutral probability');
disp('Please be aware that no arbitrage can happen, therefore 0=<d<1+r<u');
u=-1;r=-1;d=-1;
%There is no "Do-While" syntax in Matlab, therefore to start the while loop
%we can set the default value of the parameters
while ( u<0 || r<0 || u<0 || d>u || 1+r<=d || 1+r>=u ) 
prompt1=['Please enter the interest rate r:',char(10)];
r=input(prompt1);
prompt2=['Please enter the up factor u:',char(10)];
u=input(prompt2);
prompt3=['Please enter the down factor d:',char(10)];
d=input(prompt3);
    if( u<0 || r<0 || u<0 || d>u || 1+r<=d || 1+r>=u ) 
        disp('The parameters doesn''t satisfy the no arbitrage condition, please retry');
    end
end

neutralRiskH=(1+r-d)/(u-d);
neutralRiskT=(u-1-r)/(u-d);

rhOutput=['The probability of H in risk-neutral measure is ', num2str(neutralRiskH)];
rtOutput=['The probability of T in risk-neutral measure is ', num2str(neutralRiskT)];
disp('According to the binomial tree model,');
disp(rhOutput);
disp(rtOutput);
disp('Well done! Let''s now draw the pseudo binomial tree.');

%Calculate and display the binomial tree
%Here we draw a matrix as the pseudo binomial tree
promptS0=['Please set the initial value S0 for the binomial tree, S0 =', char(10)];
S0=input(promptS0);
promptN=['Please set the periods N for the binomial tree, N =', char(10)];
N=input(promptN);
disp('Good, You''ve done great job!');
disp('However, as a rookie, I can only show you a pseudo binomial tree :(');
prompt4=['Our pseudo binomial tree drawn as a matrix is:',char(10)];
disp(prompt4);
num=2^N;
pseudoTree=zeros(num,N+1);
pseudoTree(1,1)=S0;
for j=2:N+1
    index=(1:2^(N+1-j):num);
    nIndex=length(index);
    indexOld=(1:2^(N+2-j):num);
    for i=1:nIndex
        a=index(i);
        int=ceil((i-0.1)/2);
        b=indexOld(int);
        if (mod(i,2)==1)
            pseudoTree(a,j)=pseudoTree(b,j-1)*u;
        else
            pseudoTree(a,j)=pseudoTree(b,j-1)*d;
        end
    end
end
disp(pseudoTree);

%Given the payoff, calculate and display the value of a certain derivative
%and the replicating portoflio in each state.

%Derivatives of Homework
euroDigCall=zeros(num,1);
aonPut=zeros(num,1);
for i=1:num
    euroDigCall(i)=(pseudoTree(i,N+1)-3>0)*1;%ST is fixed at 3.
    aonPut(i)=(pseudoTree(i,N+1)-6<0)*pseudoTree(i,N+1);%ST is fixed at 6.
end

%we can write another indepdent function which calculates the payoff of
%some certain derivatives and quote the result, or use Matlab functions like fopen
%to input external data. Here only for simplification I include in the
%payoff of the derivatives in homework. 

disp('Based on the binomial tree model we have, let''s calculate the price of some certain derivative');
disp('And show the replication of portfolios for this derivative');
prompt5=['Please enter the payoff in the form [a1;a2;...;an], a 2^N * 1 vector. ','Remark: Currently 2^N is ', num2str(2^N),char(10)];
payoff=input(prompt5);

while (length(payoff)~=2^N)
    promptPD=['Please retry, the payoff should be a 2^N * 1 vector', char(10)];
    payoff=input(promptPD);
end
%Calculate the value of the derivative in each state
disp('The value of this derivative in each state can be also shown as a pseudo binomial tree:');
V=zeros(num,N+1);
for i=1:num
    V(i,N+1)=payoff(i);
end
j=N+1;
while (j>1)
    index=(1:2^(N+2-j):num);
    nIndex=length(index);
    indexOld=(1:2^(N+1-j):num);
    for i=1:nIndex
        a=index(i);
        b1=indexOld(2*i-1);
        b2=indexOld(2*i);
        V(a,j-1)=(V(b1,j)*neutralRiskH+V(b2,j)*neutralRiskT)/(1+r);
    end
    j=j-1;
end
disp(V);
%Calculate the MMA share of replication in each state
M=zeros(num,N+1);
j=N+1;
while (j>1)
    index=(1:2^(N+2-j):num);
    nIndex=length(index);
    indexOld=(1:2^(N+1-j):num);
    for i=1:nIndex
        a=index(i);
        b1=indexOld(2*i-1);
        b2=indexOld(2*i);
        M(a,j-1)=(V(b2,j)*u-V(b1,j)*d)/((u-d)*(1+r));
    end
    j=j-1;
end
disp('And the MMA share of replication in each state is:');
disp(M);
%Calculate the stock share of replication in each state
delta=zeros(num,N+1);
j=N+1;
while (j>1)
    index=(1:2^(N+2-j):num);
    nIndex=length(index);
    indexOld=(1:2^(N+1-j):num);
    for i=1:nIndex
        a=index(i);
        b1=indexOld(2*i-1);
        b2=indexOld(2*i);
        delta(a,j-1)=(V(b1,j)-V(b2,j))/((u-d)*pseudoTree(a,j-1));
    end
    j=j-1;
end
disp('And the stock share of replication in each state is:');
disp(delta);
disp('Great Job!');
disp('Thanks for using this program. Goodbye.');
